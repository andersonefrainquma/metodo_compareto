/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.ListaNumeros;
import static Vista.TestVectores.leerEntero;
import static Vista.TestVectores.leerFloat;
import java.util.Scanner;

/**
 *
 * @author PC-4
 */
public class Prueba_compareTo {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        ListaNumeros lista1 = new ListaNumeros(leerEntero("Digite cantidad de elementos (Lista 1): "));
        /**
         * Como vamos a realizar un proceso de almacenamiento usamos el for
         * convencional
         */
        for (int i = 0; i < lista1.length(); i++) {
            lista1.adicionar(i, leerFloat("Digite dato [" + i + "]:"));
        }

        System.out.println("Su lista 01 es :" + lista1.toString());

        ListaNumeros lista2 = new ListaNumeros(leerEntero("Digite cantidad de elementos (Lista 2): "));
        /**
         * Como vamos a realizar un proceso de almacenamiento usamos el for
         * convencional
         */
        for (int i = 0; i < lista2.length(); i++) {
            lista2.adicionar(i, leerFloat("Digite dato [" + i + "]:"));
        }

        System.out.println("Su lista 02 es :" + lista2.toString());

        int z = lista1.compareTo(lista2);
        if (z == 0) {
            System.out.println(lista1.toString() + " es igual que " + lista2.toString());
        } else if (z < 0) {
            System.out.println(lista1.toString() + " es menor que " + lista2.toString());
        } else {
            System.out.println(lista1.toString() + " es mayor que " + lista2.toString());
        }

    }
}
